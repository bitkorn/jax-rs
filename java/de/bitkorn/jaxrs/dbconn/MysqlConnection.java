/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.bitkorn.jaxrs.dbconn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.LoggerConfig;

/**
 *
 * @author Torsten Brieskorn
 */
public class MysqlConnection {

    private static final Logger LOGGER = LogManager.getLogger(LoggerConfig.RootLogger.class);
    private static Connection conn;
    private static final String USER_NAME = "root";
    private static final String PASSWORD = "sqlkomuni";
    private static final String SERVER_NAME = "localhost";
    private static final int PORT_NUMBER = 3306;

    private static void connect() {

        Properties connectionProps = new Properties();
        connectionProps.put("user", MysqlConnection.USER_NAME);
        connectionProps.put("password", MysqlConnection.PASSWORD);

        try {
            /*
             * load and register JDBC driver for MySQL
             * otherwise: No suitable driver found for jdbc:mysql
             */
            Class.forName("com.mysql.jdbc.Driver");

            MysqlConnection.conn = DriverManager.getConnection("jdbc:mysql://" + MysqlConnection.SERVER_NAME + ":" + MysqlConnection.PORT_NUMBER + "/", connectionProps);
            if (null != MysqlConnection.conn) {
                System.out.println("Connected to database");
            }
        } catch (SQLException | ClassNotFoundException ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    public static Connection getConnection() {
        try {
            if (null == MysqlConnection.conn || !MysqlConnection.conn.isValid(3)) {
                MysqlConnection.connect();
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
        }
        return MysqlConnection.conn;
    }
}
