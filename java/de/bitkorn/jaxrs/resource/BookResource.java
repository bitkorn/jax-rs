package de.bitkorn.jaxrs.resource;

import de.bitkorn.jaxrs.beans.Book;
import de.bitkorn.jaxrs.dbconn.MysqlConnection;
import java.sql.Connection;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.LoggerConfig;

/**
 * REST Web Service
 *
 * @author Torsten Brieskorn
 */
@Path("books")
public class BookResource {

    @Context
    private UriInfo context;

    private static final Logger LOGGER = LogManager.getLogger(LoggerConfig.RootLogger.class);
    private final Connection conn;

    /**
     * Creates a new instance of BookResource
     */
    public BookResource() {
        this.conn = MysqlConnection.getConnection();
    }

    /**
     * Retrieves representation of an instance of de.bitkorn.jaxrs.BookResource
     * @param id
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path ("{id}/")
    public String getJson(@PathParam("id") int id) {
        Book book = new Book();
        book.setName("Java EE 7-" + id);
        book.setAuthor("Allapow Okey");
        book.setIsbn("1234-4325-9876");
        
        return book.getString();
    }

    /**
     * PUT method for updating or creating an instance of BookResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }

    /**
     * POST method for creating an instance of BookResource
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void postJson(String content) {
        LOGGER.debug(content);
    }
}
