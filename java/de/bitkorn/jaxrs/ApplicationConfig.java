/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.bitkorn.jaxrs;

import java.util.Set;
import javax.ws.rs.core.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.LoggerConfig;

/**
 *
 * @author Torsten Brieskorn
 */
@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {

    private static final Logger LOGGER = LogManager.getLogger(LoggerConfig.RootLogger.class);

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically populated with all resources defined in the project. If required, comment out calling
     * this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(de.bitkorn.jaxrs.resource.BookResource.class);
    }

}
