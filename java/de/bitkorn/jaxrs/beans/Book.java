package de.bitkorn.jaxrs.beans;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonObject;

/**
 *
 * @author Torsten Brieskorn
 */
public class Book implements JsonString{

    private final JsonObjectBuilder jsonObjBldr;
    private JsonObject jsonObj;
    private String name;
    private String author;
    private String isbn;

    public Book() {
        this.jsonObjBldr = Json.createObjectBuilder();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.jsonObjBldr.add("name", name);
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
        this.jsonObjBldr.add("author", author);
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
        this.jsonObjBldr.add("isbn", isbn);
    }

    @Override
    public String getString() {
        this.jsonObj = this.jsonObjBldr.build();
        return this.jsonObj.toString();
    }

    @Override
    public CharSequence getChars() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ValueType getValueType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
